﻿using Fridge.Models;

namespace Fridge.Repository.Interfaces
{
    public interface IPictureRepository
    {
        void AddPicture(ProductPicture picture);
    }
}
