﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models.RoleBasedAuthorization
{
    public static class UserRoles
    {
        public const string Owner = "Owner";
        public const string Renter = "Renter";
    }
}
